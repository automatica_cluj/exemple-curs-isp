
package ui.evenimente;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


/**
 * Exemple metode de interceptare a evenimentelor.
 * @author evo
 */
class SimpleGUI extends JFrame implements ActionListener {

    int k = 0;
    JTextField tf = new JTextField(".....,.");

    SimpleGUI() {
        setSize(400, 400); //set frames size in pixels
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        JButton but1 = new JButton("Click me");

        Container cp = getContentPane();//must do this
        cp.setLayout(new FlowLayout(FlowLayout.CENTER));
        
        //clasa interna anonima
        but1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("hello");
            }
        });

        //clasa externa
        but1.addActionListener(new MyHandlerExternal(this));
        
        //clasa interna
        but1.addActionListener(new MyHandlerInternal());
        
        //implementare locala
        but1.addActionListener(this);
        
        cp.add(but1);
        cp.add(tf);
        show();
    }

    public void actionPerformed(ActionEvent e) {
        System.out.println("hello 3");
    }

    public static void main(String[] args) {
        SimpleGUI gui = new SimpleGUI();
        System.out.println("main thread coninues");

    }

    //clasa interna
    class MyHandlerInternal implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            System.out.println("hello 4");
            k++;
            tf.setText("k=" + k);
        }
    }

}//.clasa exterba

////////////////////
class MyHandlerExternal implements ActionListener {

    SimpleGUI gui;

    public MyHandlerExternal(SimpleGUI gui) {
        this.gui = gui;
    }

    public void actionPerformed(ActionEvent e) {
        System.out.println("hello 2");
        gui.k++;
        gui.tf.setText("k=" + gui.k);
    }
}
