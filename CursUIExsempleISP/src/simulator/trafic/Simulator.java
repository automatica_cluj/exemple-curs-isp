
package simulator.trafic;

import java.util.LinkedList;

public class Simulator {
    public static void main(String[] args) throws InterruptedException{
        Semafor s = new Semafor();
        Controler ctrl = new Controler(s);
        Intersectie i = new Intersectie(ctrl,s);
        int k = 0;
        while(true){
            k++;
            Thread.sleep(100);
            Masina c = new Masina();
            i.sosesteMasina(c);
            i.pleacaMasina();
            ctrl.control(k);
        }
    }
}

class Intersectie{
    private Semafor semafor1;
    private Banda banda1 = new Banda();
    private Controler ctrl;

    public Intersectie(Controler ctrl, Semafor s) {
        this.ctrl = ctrl;
        this.semafor1 = s;
    }
    
    void sosesteMasina(Masina c){
        banda1.soseste(c);
    }
    
      void pleacaMasina(){
          if(semafor1.isStare()==true)
            banda1.pleaca();
    }
     
    
}

class Banda{
    
    LinkedList<Masina> in = new LinkedList<>();
    LinkedList<Masina> out = new LinkedList<>();
    
    void soseste(Masina c){
        in.addLast(c);
        System.out.println("Soseste masina");
    }
    
    void pleaca(){
        if(in.size()>0){
         System.out.println("pleaca masina");
            out.addLast(in.removeFirst());
        }
    }
}

class Controler{
    
    Semafor s;

    public Controler(Semafor s) {
        this.s = s;
    }
    
    void control(int k){
        System.out.println("Control "+k);
        if(k%10==0)
          s.setStare(!s.isStare());
    }
}

class Semafor{
    boolean stare;

    public boolean isStare() {
        return stare;
    }

    public void setStare(boolean stare) {
        System.out.println("Schimb stare "+stare);
        this.stare = stare;
    }
    
}

class Masina{
    
}