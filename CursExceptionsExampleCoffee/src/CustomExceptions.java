
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CustomExceptions {

    public static void main(String[] args) {
        CoffeMaker m1 = new CoffeMaker();
        CoffeDrinker cd1 = new CoffeDrinker(m1);
        //  try {

        for (int i = 0; i < 10; i++) {
        
                cd1.haveACoffe();
         
        }

      ///  } catch (ToMuchCoffeForToday ex) {
        //      System.out.println("You are ready for your work day!");
        //  }
        System.out.println("Do something!");
    }
}

class CoffeDrinker {

    private static final int MAX_COFFE = 90;
    private int quantity;
    private CoffeMaker maker;

    public CoffeDrinker(CoffeMaker maker) {
        this.maker = maker;
    }

    void haveACoffe(){
            //throws ToMuchCoffeForToday {
        Coffe c = maker.getCoffe();
        quantity += c.getCofeein();
        if (quantity >= MAX_COFFE) {
            throw new ToMuchCoffeForToday(quantity, "Pleas stop!");
        }
        c.drink();
    }
}

class CoffeMaker {

    private CoffeTank ctank;
    private WaterTank wtank;

    CoffeMaker() {
        ctank = new CoffeTank();
        wtank = new WaterTank();
    }

    Coffe getCoffe() {
        return new Coffe(wtank.getWater(), ctank.getCoffe());
    }
}

class Coffe {

    private int water;
    private int cofeein;

    public Coffe(int water, int cofeein) {
        this.water = water;
        this.cofeein = cofeein;
    }

    int getCofeein() {
        return cofeein;
    }

    void drink() {
        System.out.println("Drink " + this);
    }

    @Override
    public String toString() {
        return "Coffe{" + "water=" + water + ", cofeein=" + cofeein + '}';
    }

}

class CoffeTank {

    int getCoffe() {
        Random r = new Random();
        return r.nextInt(30);

    }
}

class WaterTank {

    int getWater() {
        Random r = new Random();
        return r.nextInt(100);

    }
}

class ToMuchCoffeForToday extends RuntimeException {

    int quantity;

    public ToMuchCoffeForToday(int quantity, String message) {
        super(message);
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

}
